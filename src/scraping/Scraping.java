/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scraping;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Arrays;
import javax.swing.JOptionPane;

/**
 *
 * @author goodwillhunting
 */
public class Scraping {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        // PASS
        
      
       
    }

    
     static void phones_tablets(){
        
             try {
            Document doc = Jsoup.connect("https://www.jumia.co.ke/phones-tablets/").userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0").timeout(0).get();
            Elements links = doc.select("div.sku");
            String csvFile = "/home/goodwillhunting/Desktop/phoneTablets.csv";
            FileWriter fw = new FileWriter(csvFile,true);

             CSVUtils.writeLine(fw, Arrays.asList("Name", "Price"));
            
            int count=0; 
            for (Element s : links) {
                Elements n = s.select("span.name");
                String name=n.text();               
                Elements p=s.select("span.price").eq(0);
                String price=p.text();
                count++;
                System.out.println(name+" "+price);
                
                List<String> list = new ArrayList<>();
                list.add(name);
                list.add(price);
                
                CSVUtils.writeLine(fw, list);

            }
             fw.flush();
             fw.close();
             JOptionPane.showMessageDialog(null,"Record Saved");
            
        } catch (Exception ex) {
            Logger.getLogger(Scraping.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     
     static void electronics(){
        
             try {
            Document doc = Jsoup.connect("https://www.jumia.co.ke/electronics/").userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0").timeout(0).get();
            Elements links = doc.select("div.sku");
            String csvFile = "/home/goodwillhunting/Desktop/electronics.csv";
            FileWriter fw = new FileWriter(csvFile,true);

             CSVUtils.writeLine(fw, Arrays.asList("Name", "Price"));
            
            int count=0; 
            for (Element s : links) {
                Elements n = s.select("span.name");
                String name=n.text();               
                Elements p=s.select("span.price").eq(0);
                String price=p.text();
                count++;
                System.out.println(name+" "+price);
                
                List<String> list = new ArrayList<>();
                list.add(name);
                list.add(price);
                
                CSVUtils.writeLine(fw, list);

            }
             fw.flush();
             fw.close();
             JOptionPane.showMessageDialog(null,"Record Saved");
            
        } catch (Exception ex) {
            Logger.getLogger(Scraping.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     
    static void computing(){
        
             try {
            Document doc = Jsoup.connect("https://www.jumia.co.ke/computing/").userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0").timeout(0).get();
            Elements links = doc.select("div.sku");
            String csvFile = "/home/goodwillhunting/Desktop/computing.csv";
            FileWriter fw = new FileWriter(csvFile,true);

             CSVUtils.writeLine(fw, Arrays.asList("Name", "Price"));
            
            int count=0; 
            for (Element s : links) {
                Elements n = s.select("span.name");
                String name=n.text();               
                Elements p=s.select("span.price").eq(0);
                String price=p.text();
                count++;
                System.out.println(name+" "+price);
                
                List<String> list = new ArrayList<>();
                list.add(name);
                list.add(price);
                
                CSVUtils.writeLine(fw, list);

            }
             fw.flush();
             fw.close();
             JOptionPane.showMessageDialog(null,"Record Saved");
            
        } catch (Exception ex) {
            Logger.getLogger(Scraping.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     
    static void home_office(){
        
             try {
            Document doc = Jsoup.connect("https://www.jumia.co.ke/home-office/").userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0").timeout(0).get();
            Elements links = doc.select("div.sku");
            String csvFile = "/home/goodwillhunting/Desktop/homeoffice.csv";
            FileWriter fw = new FileWriter(csvFile,true);

             CSVUtils.writeLine(fw, Arrays.asList("Name", "Price"));
            
            int count=0; 
            for (Element s : links) {
                Elements n = s.select("span.name");
                String name=n.text();               
                Elements p=s.select("span.price").eq(0);
                String price=p.text();
                count++;
                System.out.println(name+" "+price);
                
                List<String> list = new ArrayList<>();
                list.add(name);
                list.add(price);
                
                CSVUtils.writeLine(fw, list);

            }
             fw.flush();
             fw.close();
             JOptionPane.showMessageDialog(null,"Record Saved");
            
        } catch (Exception ex) {
            Logger.getLogger(Scraping.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
    
    static void health_beauty(){
        
             try {
            Document doc = Jsoup.connect("https://www.jumia.co.ke/health-beauty/").userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0").timeout(0).get();
            Elements links = doc.select("div.sku");
            String csvFile = "/home/goodwillhunting/Desktop/healthbeauty.csv";
            FileWriter fw = new FileWriter(csvFile,true);

             CSVUtils.writeLine(fw, Arrays.asList("Name", "Price"));
            
            int count=0; 
            for (Element s : links) {
                Elements n = s.select("span.name");
                String name=n.text();               
                Elements p=s.select("span.price").eq(0);
                String price=p.text();
                count++;
                System.out.println(name+" "+price);
                
                List<String> list = new ArrayList<>();
                list.add(name);
                list.add(price);
                
                CSVUtils.writeLine(fw, list);

            }
             fw.flush();
             fw.close();
             JOptionPane.showMessageDialog(null,"Record Saved");
            
        } catch (Exception ex) {
            Logger.getLogger(Scraping.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     
    static void groceries(){
        
             try {
            Document doc = Jsoup.connect("https://www.jumia.co.ke/groceries/").userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0").timeout(0).get();
            Elements links = doc.select("div.sku");
            String csvFile = "/home/goodwillhunting/Desktop/groceries.csv";
            FileWriter fw = new FileWriter(csvFile,true);

             CSVUtils.writeLine(fw, Arrays.asList("Name", "Price"));
            
            int count=0; 
            for (Element s : links) {
                Elements n = s.select("span.name");
                String name=n.text();               
                Elements p=s.select("span.price").eq(0);
                String price=p.text();
                count++;
                System.out.println(name+" "+price);
                
                List<String> list = new ArrayList<>();
                list.add(name);
                list.add(price);
                
                CSVUtils.writeLine(fw, list);

            }
             fw.flush();
             fw.close();
             JOptionPane.showMessageDialog(null,"Record Saved");
            
        } catch (Exception ex) {
            Logger.getLogger(Scraping.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    static void baby_products(){
        
             try {
            Document doc = Jsoup.connect("https://www.jumia.co.ke/baby-products/").userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0").timeout(0).get();
            Elements links = doc.select("div.sku");
            String csvFile = "/home/goodwillhunting/Desktop/babyproducts.csv";
            FileWriter fw = new FileWriter(csvFile,true);

             CSVUtils.writeLine(fw, Arrays.asList("Name", "Price"));
            
            int count=0; 
            for (Element s : links) {
                Elements n = s.select("span.name");
                String name=n.text();               
                Elements p=s.select("span.price").eq(0);
                String price=p.text();
                count++;
                System.out.println(name+" "+price);
                
                List<String> list = new ArrayList<>();
                list.add(name);
                list.add(price);
                
                CSVUtils.writeLine(fw, list);

            }
             fw.flush();
             fw.close();
             JOptionPane.showMessageDialog(null,"Record Saved");
            
        } catch (Exception ex) {
            Logger.getLogger(Scraping.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    static void fashion(){
        
             try {
            Document doc = Jsoup.connect("https://www.jumia.co.ke/category-fashion-by-jumia/").userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0").timeout(0).get();
            Elements links = doc.select("div.sku");
            String csvFile = "/home/goodwillhunting/Desktop/fashion.csv";
            FileWriter fw = new FileWriter(csvFile,true);

             CSVUtils.writeLine(fw, Arrays.asList("Name", "Price"));
            
            int count=0; 
            for (Element s : links) {
                Elements n = s.select("span.name");
                String name=n.text();               
                Elements p=s.select("span.price").eq(0);
                String price=p.text();
                count++;
                System.out.println(name+" "+price);
                
                List<String> list = new ArrayList<>();
                list.add(name);
                list.add(price);
                
                CSVUtils.writeLine(fw, list);

            }
             fw.flush();
             fw.close();
             JOptionPane.showMessageDialog(null,"Record Saved");
            
        } catch (Exception ex) {
            Logger.getLogger(Scraping.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    static void automobile(){
        
             try {
            Document doc = Jsoup.connect("https://www.jumia.co.ke/automobile/").userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0").timeout(0).get();
            Elements links = doc.select("div.sku");
            
            String csvFile = "/home/goodwillhunting/Desktop/automobile.csv";
            FileWriter fw = new FileWriter(csvFile,true);

             CSVUtils.writeLine(fw, Arrays.asList("Name", "Price"));
            
            int count=0; 
            for (Element s : links) {
                Elements n = s.select("span.name");
                String name=n.text();               
                Elements p=s.select("span.price").eq(0);
                String price=p.text();
                count++;
                System.out.println(name+" "+price);
                
                List<String> list = new ArrayList<>();
                list.add(name);
                list.add(price);
                
                CSVUtils.writeLine(fw, list);

            }
             fw.flush();
             fw.close();
             JOptionPane.showMessageDialog(null,"Record Saved");
            
        } catch (Exception ex) {
            Logger.getLogger(Scraping.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    static void video_games(){
        
             try {
            Document doc = Jsoup.connect("https://www.jumia.co.ke/video-games/").userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0").timeout(0).get();
            Elements links = doc.select("div.sku");
            String csvFile = "/home/goodwillhunting/Desktop/videogames.csv";
            FileWriter fw = new FileWriter(csvFile,true);

             CSVUtils.writeLine(fw, Arrays.asList("Name", "Price"));
            
            int count=0; 
            for (Element s : links) {
                Elements n = s.select("span.name");
                String name=n.text();               
                Elements p=s.select("span.price").eq(0);
                String price=p.text();
                count++;
                System.out.println(name+" "+price);
                
                List<String> list = new ArrayList<>();
                list.add(name);
                list.add(price);
                
                CSVUtils.writeLine(fw, list);

            }
             fw.flush();
             fw.close();
             JOptionPane.showMessageDialog(null,"Record Saved");
            
        } catch (Exception ex) {
            Logger.getLogger(Scraping.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    

    
    
   
}